Java implementation for 'Strong' and 'Weak' text, binary, integer and double encryption/decryption using
many different algorithms provided by the Java 

If you do not already have the Java JCE Policy installed, you must:

1:
	Make sure that it is legal in your current country to use strong encryption algorithms, as many 3rd
	world and authoritarian countries forbid their usage by civilians. I am not at fault for distributing 
	this software. Use at your own risk.


2:
	Download the Java JCE for your current version of java (5, 6, 7..etc) or install the Java JCE 8 I have
	included in this package. Instructions are in the 'README.txt' file of the 'UnlimitedJCEPolicyJDK8' 
	folder.
	
3:
	Run the program, or in terminal type 'java -jar JavaGUIAllPurposeEncryption'
	
	