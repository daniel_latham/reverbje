package uab.edu.l3li3l.app.gui;

/**
 * Starts up the GUI for this weak-encryption program
 * This program implements the Jasypt encryption library
 * @version 0.0.1
 * @serial GHHVTZckaSJHAVZn3brapVEK
 * @author dan
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		mf = new MainFrame("GUI Encryptor");
	}

	protected static MainFrame mf;
}
