package uab.edu.l3li3l.app.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RadioActionListener implements ActionListener {

	public RadioActionListener() {
		//Nothing
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//Displays text on radio button
		System.out.println(e.getActionCommand());
		this.currentFunction = e.getActionCommand();
		///TODO Remove previous clicked button
		Main.mf.unclickRadios(currentFunction);

	}

	private String currentFunction;
}
