/**
 * 
 */
package uab.edu.l3li3l.app.gui;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

import org.jasypt.registry.AlgorithmRegistry;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Set;

/**
 * @author dan
 *
 */
public class MainFrame extends JFrame {

	/**
	 * Temp ID
	 */
	private static final long serialVersionUID = 5708836001372874952L;

	/**
	 * Constructor
	 */
	protected MainFrame(String title) 
	{
		super(title);
		this.setLayout(new BorderLayout());
		//'kill -9 this' on closing of this
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Preferred size for pseudo-minimized
		Dimension d = new Dimension(width,height);
		this.setPreferredSize(d);
		//Set maximized
		this.setExtendedState(this.getExtendedState()|JFrame.MAXIMIZED_BOTH );
		addInterface();
		//unclick all radios except md5
		setWindowResizeListener();
		this.pack();
		this.show(true);
	}
	/**
	 * Adds the input panels to this frame
	 */
	private void addTextPanels()
	{
		this.scrollP.setVerticalScrollBar(this.scrollB);
		this.scrollP.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		//Add text line numbers to scrollP
		TextLineNumber tln = new TextLineNumber(this.output);
		this.scrollP.setRowHeaderView(tln);
		//set this.password to one line
		this.password.getDocument().putProperty("filterNewlines", Boolean.TRUE);
		//ADD scroll pane
		this.add(this.scrollP, BorderLayout.CENTER);
		//Add password and button
		this.bPanel.setLayout(new BorderLayout());
		this.bPanel.add(this.password, BorderLayout.CENTER);
		this.bPanel.add(this.enter, BorderLayout.EAST);
		this.add(this.bPanel, BorderLayout.SOUTH);
		
	}
	/**
	 * 
	 * @param except String to decide which one to keep popped up, optionally null
	 */
	protected void unclickRadios(String except)
	{
		int j = 0;
		for (JRadioButtonMenuItem button: this.buttons)
		{
			if (!button.getText().equalsIgnoreCase(except))
			{
				button.setSelected(false);
			}
			else
			{
				if (j == 0 || j == 1)
				{
					changeMode(this.HASH);
					System.out.println("HASH J = "+j);
				}
				else if (j == 2 || j == 3)
				{
					changeMode(this.ENCRYPT);
					System.out.println("ENCRYPT J = "+j);
				}
				else if (j == 4 || j == 5)
				{
					changeMode(this.DECRYPT);
					System.out.println("DECRYPT J = "+j);
				}
			}
			j++;
			
		}
		
	}
	/**
	 * TODO: made radio buttons class variables
	 * Adds the drop-down menu to the upper lefthand corner of this Frame
	 */
	private void addMenu()
	{
		//Main buttons
		String[] barButtons = {"File","Mode"};
		///FILE MENU
		JMenu fileM = new JMenu(barButtons[0]);
		JMenu openM = new JMenu("Open");
		//TEXT OPEN
		JMenuItem textMI = new JMenuItem("Text Document");
		textMI.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				openTextFile();
			}
		});
		//BINARY OPEN
		JMenuItem binaryMI = new JMenuItem("Binary Document");
		binaryMI.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				openBinaryFile();
			}
		});
		openM.add(textMI);
		openM.add(binaryMI);
		fileM.add(openM);
		///TODO SAVE and EXIT buttons
		///MODE menu
		JMenu modeM = new JMenu(barButtons[1]);
		///TODO All hash functions
		JMenu hashM = new JMenu("Hash");
		this.md5.addActionListener(this.radAct);
		hashM.add(this.md5);
		this.sha1.addActionListener(this.radAct);
		hashM.add(this.sha1);
		///TODO All encrypt functions
		JMenu encryptM = new JMenu("Encrypt");
		this.weakE.addActionListener(this.radAct);
		encryptM.add(this.weakE);
		this.strongE.addActionListener(this.radAct);
		encryptM.add(this.strongE);
		
		///TODO ALl Decrypt functions
		JMenu decryptM = new JMenu("Decrypt");
		this.weakD.addActionListener(this.radAct);
		decryptM.add(this.weakD);
		this.strongD.addActionListener(this.radAct);
		decryptM.add(this.strongD);
		
		//Add all buttons to mode
		modeM.add(hashM);
		modeM.add(encryptM);
		modeM.add(decryptM);
		//Add all menus to bar sequentially
		this.bar.add(fileM);
		this.bar.add(modeM);
		
		this.add(this.bar, BorderLayout.NORTH);
	}
	/**
	 * Adds all the parts of the panels and menus to this frame
	 */
	private void addInterface()
	{
		addMenu();
		addTextPanels();
	}
	private void openTextFile()
	{
		//Gets the current working directory
		final String dir = System.getProperty("user.dir");
		
		JFileChooser chooser = new JFileChooser(dir);
		
		
		int returnVal = chooser.showOpenDialog(chooser);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			String filePath = chooser.getSelectedFile().getPath();
			try
			{
				File file = new File(filePath);
			    BufferedReader br = new BufferedReader(new FileReader(file));
			    StringBuilder sb = new StringBuilder();
			    String line = br.readLine();

			    while (line != null) 
			    {
			    	sb.append(line);
			    	sb.append(System.lineSeparator());
			    	line = br.readLine();
			    }
			    br.close();
			    
			    this.text = sb.toString();
			    
			    this.output.setText(text);
			}
			catch (Exception e)
			{
				//TODO popup window
				StackTraceElement[] elements = Thread.currentThread().getStackTrace();
				for (int i = 0; i < elements.length; i++)
				{
					System.out.println(elements[i]);
				}
			}
		}
	}
	private void openBinaryFile()
	{
		//Gets the current working directory
		final String dir = System.getProperty("user.dir");
		
		JFileChooser chooser = new JFileChooser(dir);
		
		
		int returnVal = chooser.showOpenDialog(chooser);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			String filePath = chooser.getSelectedFile().getPath();
			try
			{
				Path path = Paths.get(filePath, new String[0]);
			    this.bin = Files.readAllBytes(path);
			    this.text = bytesToHex(this.bin);
			    this.output.setText(this.text);
			}
			catch (Exception e)
			{
				//TODO popup window
				StackTraceElement[] elements = Thread.currentThread().getStackTrace();
				for (int i = 0; i < elements.length; i++)
				{
					System.out.println(elements[i]);
				}
			}
		}
	}
	/**
	 * Changes the text on the enter button
	 */
	private void changeEnterButton()
	{
		//System.out.println("MODE = "+this.MODE);
		
		if(this.MODE == this.HASH) this.enter.setText("Hash");
		else if(this.MODE == this.ENCRYPT) this.enter.setText("Encrypt");
		else this.enter.setText("Decrypt");
		
		pack();
		repaint();
	}
	/**
	 * Changes the mode of this frame
	 */
	private void changeMode(int mode)
	{
		this.MODE = mode;
		changeEnterButton();
	}
	/**
	 * 
	 * @param bytes array
	 * @return String of bytes concatenated 
	 */
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	/**
	 * Returns a list of all the algorithms
	 * TODO Return an array of strings
	 */
	private void getAllAlgorithmNames()
	{
		//Hash functions
		Set algorithmsD = AlgorithmRegistry.getAllDigestAlgorithms();
		//Password based functions
		Set algorithmsP = AlgorithmRegistry.getAllPBEAlgorithms();
		//Both iterators
		Iterator iterD = algorithmsD.iterator();
		Iterator iterP = algorithmsP.iterator();
		
		while(iterP.hasNext())
		{
			System.out.println(iterP.next());
		}
	}
	/**
	 * Adds resize window listener for when this.ismage image
	 */
	private void setWindowResizeListener()
	{
		this.addComponentListener( new ComponentListener(){

			@Override
			public void componentResized(ComponentEvent e) {
				width = getWidth();
				height = getHeight();
				Dimension d = new Dimension(width,height);
				setPreferredSize(d);
			}

			@Override
			public void componentMoved(ComponentEvent e) {
				
				
			}

			@Override
			public void componentShown(ComponentEvent e) {
				
				
			}

			@Override
			public void componentHidden(ComponentEvent e) {
				
				
			}
		});
	}
	
	private byte[] bin;
	private String text;
	
	//Radio buttons
	private final JRadioButtonMenuItem md5 = new JRadioButtonMenuItem("MD5");
	private final JRadioButtonMenuItem sha1 = new JRadioButtonMenuItem("SHA-1");
	private final JRadioButtonMenuItem weakE = new JRadioButtonMenuItem("Weak Encryption");
	private final JRadioButtonMenuItem strongE = new JRadioButtonMenuItem("Strong Encryption");
	private final JRadioButtonMenuItem weakD = new JRadioButtonMenuItem("Weak Decryption");
	private final JRadioButtonMenuItem strongD = new JRadioButtonMenuItem("Strong Decryption");
	private final JRadioButtonMenuItem[] buttons = {md5, sha1, weakE, strongE, weakD, strongD};
	//
	private final RadioActionListener radAct = new RadioActionListener();
	private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
	
	private final JButton enter = new JButton("Enter");
	private final JTextArea password = new JTextArea();
	private final JPanel bPanel = new JPanel();
	
	private final JMenuBar bar = new JMenuBar();
	
	private final JEditorPane output = new JEditorPane();
	private final JScrollPane scrollP = new JScrollPane(output);
	private final JScrollBar scrollB = new JScrollBar();
	
	private int width = 500;
	private int height = 500;
	
	///TODO remove this, just make it action based, start with HASH - MD5 clicked on
	private int MODE = 0;
	private final int HASH = 0;
	private final int ENCRYPT = 1;
	private final int DECRYPT = 2;

}
